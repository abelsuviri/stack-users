package com.abelsuviri.viewmodel.mock

/**
 * @author Abel Suviri
 */

object MockJson {
    const val mockResponse = "{" +
            "\"items\": [{" +
            "\"badge_counts\": {" +
            "\"bronze\": 15," +
            "\"silver\": 5," +
            "\"gold\": 0" +
            "}," +
            "\"account_id\": 3808698," +
            "\"is_employee\": false," +
            "\"last_modified_date\": 1573680815," +
            "\"last_access_date\": 1586940771," +
            "\"reputation_change_year\": 20," +
            "\"reputation_change_quarter\": 0," +
            "\"reputation_change_month\": 0," +
            "\"reputation_change_week\": 0," +
            "\"reputation_change_day\": 0," +
            "\"reputation\": 486," +
            "\"creation_date\": 1388840928," +
            "\"user_type\": \"registered\"," +
            "\"user_id\": 3160317," +
            "\"accept_rate\": 86," +
            "\"location\": \"Delhi, India\"," +
            "\"website_url\": \"\"," +
            "\"link\": \"https://stackoverflow.com/users/3160317/1shubhamjoshi1\"," +
            "\"profile_image\": \"https://graph.facebook.com/100005763070491/picture?type=large\"," +
            "\"display_name\": \"1shubhamjoshi1\"" +
            "}]}"
}