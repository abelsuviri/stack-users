package com.abelsuviri.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.abelsuviri.data.model.ResponseModel
import com.abelsuviri.data.network.StackService
import com.abelsuviri.viewmodel.mock.MockJson
import com.abelsuviri.viewmodel.rules.RxSchedulerRule
import com.google.gson.Gson
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


/**
 * @author Abel Suviri
 */
class MainViewModelTest {

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var service: StackService

    private lateinit var gson: Gson

    @InjectMocks
    private lateinit var mainViewModel: MainViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        gson = Gson()
    }

    @Test
    fun test_get_users_successfully() {
        val responseModel: ResponseModel = gson.fromJson(MockJson.mockResponse, ResponseModel::class.java)

        Mockito.`when`(service.getUser("")).thenReturn(Observable.just(responseModel))

        mainViewModel.getUsers("")

        Assert.assertEquals(mainViewModel.usersList.value, responseModel.users)
        Assert.assertEquals(mainViewModel.isLoading.value, false)
        Assert.assertEquals(mainViewModel.hasFailed.value, false)
    }

    @Test
    fun test_get_users_unsuccessfully() {
        Mockito.`when`(service.getUser("")).thenReturn(Observable.error(Throwable()))

        mainViewModel.getUsers("")

        Assert.assertEquals(mainViewModel.usersList.value, null)
        Assert.assertEquals(mainViewModel.isLoading.value, false)
        Assert.assertEquals(mainViewModel.hasFailed.value, true)
    }
}
