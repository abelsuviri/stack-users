package com.abelsuviri.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abelsuviri.data.model.UserModel
import com.abelsuviri.data.network.StackService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author Abel Suviri
 */

class MainViewModel @Inject constructor(private val stackService: StackService) : ViewModel() {

    val usersList: MutableLiveData<List<UserModel>> = MutableLiveData()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val hasFailed: MutableLiveData<Boolean> = MutableLiveData()

    fun getUsers(username: String) {
        isLoading.value = true
        hasFailed.value = false

        val users = stackService.getUser(username)
        users.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ userList ->
                usersList.postValue(userList.users)
                setSuccess()
            }, {
                setFailure()
            })
    }

    fun setSuccess() {
        isLoading.value = false
        hasFailed.value = false
    }

    fun setFailure() {
        isLoading.value = false
        hasFailed.value = true
    }
}