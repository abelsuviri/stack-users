package com.abelsuviri.stackusers

import android.app.Activity
import android.app.Application
import com.abelsuviri.stackusers.di.components.AppComponent
import com.abelsuviri.stackusers.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class StackUsersApp : Application(), HasActivityInjector {
    @Inject
    lateinit var activityDispatchingInjector: DispatchingAndroidInjector<Activity>

    val component: AppComponent = DaggerAppComponent.builder().build()

    override fun onCreate() {
        super.onCreate()

        component.inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingInjector
    }
}