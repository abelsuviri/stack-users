package com.abelsuviri.stackusers.di.components

import com.abelsuviri.stackusers.StackUsersApp
import com.abelsuviri.stackusers.di.module.ActivityBuilderModule
import com.abelsuviri.stackusers.di.module.ServiceModule
import com.abelsuviri.stackusers.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * @author Abel Suviri
 */

@Singleton
@Component(modules = [ActivityBuilderModule::class, AndroidInjectionModule::class, ServiceModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(stackUsersApp: StackUsersApp)
}