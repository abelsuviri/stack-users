package com.abelsuviri.stackusers.di.module

import com.abelsuviri.stackusers.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Abel Suviri
 */

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity
}