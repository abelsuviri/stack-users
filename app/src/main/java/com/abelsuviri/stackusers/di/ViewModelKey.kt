package com.abelsuviri.stackusers.di

import androidx.lifecycle.ViewModel

import dagger.MapKey
import kotlin.reflect.KClass

/**
 * @author Abel Suviri
 */

@MustBeDocumented
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)