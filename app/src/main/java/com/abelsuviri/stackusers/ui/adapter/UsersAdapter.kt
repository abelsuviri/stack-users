package com.abelsuviri.stackusers.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abelsuviri.data.model.UserModel
import com.abelsuviri.stackusers.R
import com.abelsuviri.stackusers.ui.adapter.viewholder.ItemClick
import com.abelsuviri.stackusers.ui.adapter.viewholder.UserViewHolder

/**
 * @author Abel Suviri
 */

class UsersAdapter constructor(private val userList: List<UserModel>,
                               private val itemClick: ItemClick) : RecyclerView.Adapter<UserViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.user_item_view, parent, false)

        return UserViewHolder(itemView, itemClick)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val item = userList[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return userList.size
    }
}