package com.abelsuviri.stackusers.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.abelsuviri.data.model.UserModel
import com.abelsuviri.stackusers.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user_details.*
import java.text.SimpleDateFormat
import java.util.*

class UserDetailsActivity : AppCompatActivity() {

    private lateinit var userModel: UserModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)

        userModel = intent.getParcelableExtra(USER_MODEL_EXTRA)!!

        fillViews()
    }

    private fun fillViews() {
        Picasso.get()
            .load(userModel.avatar)
            .placeholder(ResourcesCompat.getDrawable(resources, R.drawable.ic_launcher_foreground, null)!!)
            .into(avatarImageView)
        usernameTextView.text = userModel.username
        reputationTextView.text = userModel.reputation.toString()

        badgesTextView.text = getString(R.string.badges, userModel.badges?.gold, userModel.badges?.silver,
            userModel.badges?.bronze)

        if (userModel.location != null) {
            locationTextView.text = userModel.location
        } else {
            locationLabel.visibility = View.GONE
            locationTextView.visibility = View.GONE
        }


        if (userModel.age != null) {
            ageTextView.text = userModel.age.toString()
        } else {
            ageTextView.visibility = View.GONE
            ageLabel.visibility = View.GONE
        }

        creationbDateTextView.text = getDateTime(userModel.creationDate)

    }

    private fun getDateTime(dateInt: Int): String? {
        try {
            val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            val netDate = Date(dateInt.toLong() * 1000)
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }
}

const val USER_MODEL_EXTRA = "user_model_extra"
