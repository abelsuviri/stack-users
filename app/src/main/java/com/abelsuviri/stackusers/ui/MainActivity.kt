package com.abelsuviri.stackusers.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abelsuviri.data.model.UserModel
import com.abelsuviri.stackusers.R
import com.abelsuviri.stackusers.ui.adapter.UsersAdapter
import com.abelsuviri.stackusers.ui.adapter.viewholder.ItemClick
import com.abelsuviri.viewmodel.MainViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/**
 * @author Abel Suviri
 */

class MainActivity : AppCompatActivity(), ItemClick {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]

        initViews()
        subscribeLiveData()
    }

    private fun initViews() {
        userListRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        userListRecyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        searchButton.setOnClickListener {
            mainViewModel.getUsers(nameEditText.text.toString())
        }
    }

    private fun subscribeLiveData() {
        mainViewModel.isLoading.observe(this, Observer<Boolean> { isLoading -> loadingLayout.visibility = if (isLoading!!) View.VISIBLE else View.GONE })
        mainViewModel.hasFailed.observe(this, Observer<Boolean> { hasFailed -> if (hasFailed!!) showRetryDialog() })
        mainViewModel.usersList.observe(this, Observer<List<UserModel>> { userList ->
            if (userList.isNotEmpty()) {
                val cappedList = userList.take(20)
                userListRecyclerView.adapter = UsersAdapter(cappedList, this)
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(resources.getString(R.string.request_no_result))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(android.R.string.ok)) { dialog, i ->
                    dialog.dismiss()
                }.show()
            }
        })
    }

    override fun onItemClick(user: UserModel) {
        val intent = Intent(this, UserDetailsActivity::class.java)
        intent.putExtra(USER_MODEL_EXTRA, user)
        startActivity(intent)
    }

    private fun showRetryDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(resources.getString(R.string.request_error))
        builder.setCancelable(false)
        builder.setPositiveButton(resources.getString(R.string.retry)) { dialog, i ->
            dialog.dismiss()
        }.show()
    }
}
