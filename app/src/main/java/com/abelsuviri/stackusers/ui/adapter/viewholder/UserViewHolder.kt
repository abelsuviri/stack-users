package com.abelsuviri.stackusers.ui.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.abelsuviri.data.model.UserModel
import kotlinx.android.synthetic.main.user_item_view.view.*

/**
 * @author Abel Suviri
 */

class UserViewHolder constructor(view: View, private val itemClick: ItemClick) : RecyclerView.ViewHolder(view) {
    fun bind(user: UserModel) {
        itemView.reputation.text = user.reputation.toString()
        itemView.username.text = user.username

        itemView.setOnClickListener { itemClick.onItemClick(user) }
    }
}