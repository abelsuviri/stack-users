package com.abelsuviri.stackusers.ui.adapter.viewholder

import com.abelsuviri.data.model.UserModel

/**
 * @author Abel Suviri
 */

interface ItemClick {
    fun onItemClick(user: UserModel)
}