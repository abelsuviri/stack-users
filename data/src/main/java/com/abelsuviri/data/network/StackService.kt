package com.abelsuviri.data.network

import com.abelsuviri.data.model.ResponseModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Abel Suviri
 */

interface StackService {
    @GET("/2.2/users?order=asc&sort=name&site=stackoverflow")
    fun getUser(@Query("inname") username: String): Observable<ResponseModel>
}