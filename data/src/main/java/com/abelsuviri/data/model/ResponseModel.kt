package com.abelsuviri.data.model

import com.google.gson.annotations.SerializedName

/**
 * @author Abel Suviri
 */

data class ResponseModel (
    @SerializedName("items") val users: List<UserModel>
)