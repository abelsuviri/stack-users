package com.abelsuviri.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * @author Abel Suviri
 */

@Parcelize
data class UserModel(
    @SerializedName("display_name") val username: String,
    @SerializedName("reputation") val reputation: Int,
    @SerializedName("profile_image") val avatar: String,
    @SerializedName("badge_counts") val badges: BadgeModel,
    @SerializedName("location") val location: String?,
    @SerializedName("age") val age: Int?,
    @SerializedName("creation_date") val creationDate: Int) : Parcelable
{
}